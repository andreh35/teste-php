import VendaIndex from './components/venda/index.vue';
import VendaCreate from './components/venda/create.vue';

import VendedorIndex from './components/vendedor/index.vue';
import VendedorCreate from './components/vendedor/create.vue';

import VendedorVendaIndex from './components/vendedorvenda/index.vue';


export const routes = [
    {
        path: '/venda',
        component: VendaIndex,
        name: "VendaIndex"
    },

    {
        path: '/venda/create',
        component: VendaCreate,
        name: "VendaCreate"
    },

    {
        path: '/vendedor',
        component: VendedorIndex,
        name: "VendedorIndex"
    },

    {
        path: '/vendedor/create',
        component: VendedorCreate,
        name: "VendedorCreate"
    },

    {
        path: '/vendedorvenda/:id',
        component: VendedorVendaIndex,
        name: "VendedorVendaIndex"
    },
];
