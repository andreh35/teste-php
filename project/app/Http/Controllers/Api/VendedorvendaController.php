<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vendedor;
use Illuminate\Http\Request;

class VendedorvendaController extends Controller
{
    public function index()
    {
        $vendedorvenda = self::retornaVendasVendedor();
        return response()->json($vendedorvenda);
    }

    public function show($id)
    {
        $vendedorvenda = self::retornaVendasVendedor($id);
        return response()->json($vendedorvenda);
    }

    public function retornaVendasVendedor($id = null)
    {
        $vendedorvenda = Vendedor::select('vendedores.id',
            'vendedores.nome',
            'vendedores.email',
            'vendas.valor_venda',
            Vendedor::raw('format((vendas.valor_venda * 0.085),2) as comissao'),
            Vendedor::raw('DATE_FORMAT(vendas.created_at, "%d/%m/%Y") as data_venda'))
            ->join('vendas', 'vendas.vendedor_id', '=', 'vendedores.id')
            ->when($id, function ($query, $id) {
                return $query->where('vendas.vendedor_id', $id);
            })
            ->get();

        return $vendedorvenda;
    }
}
