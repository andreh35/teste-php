<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vendedor;

class VendedorController extends Controller
{
    public function index()
    {
        $vendedor = Vendedor::select('vendedores.id',
        'vendedores.nome',
        'vendedores.email',
        Vendedor::raw('(select sum(vendas.valor_venda)
        from vendas where vendas.vendedor_id = vendedores.id )* 0.085
        as comissao'))
        ->leftJoin('vendas','vendas.vendedor_id','=','vendedores.id')
        ->groupby('vendedores.id')
        ->get();
        //$seller = Seller::all();
        return response()->json($vendedor);
    }

    public function store(Request $request)
    {
        $vendedor = Vendedor::create($request->all());
        return response()->json([
            'status' => 'success',
            'value'   => $vendedor
        ]);
    }

    public function show($id)
    {
        $vendedor = Vendedor::find($id);
        return response()->json($vendedor);
    }

    public function update(Request $request, $id)
    {
        $vendedor = Vendedor::find($id);
        $vendedor->update($vendedor->all());
        return response()->json([
            'status' => 'success',
            'value'   => $vendedor
        ]);
    }

    public function destroy($id)
    {
        $vendedor = Vendedor::find($id);
        $vendedor->delete();
        return response()->json('Vendedor excluido com sucesso');
    }
}
