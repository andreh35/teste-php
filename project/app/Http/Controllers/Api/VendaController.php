<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Venda;

class VendaController extends Controller
{
    public function index()
    {
        $venda = Venda::select('vendedores.nome','vendas.valor_venda',
        'vendas.comissao','vendas.created_at')
        ->join('vendedores', 'vendedores.id','=','vendas.vendedor_id')
        ->orderby('vendas.id', 'desc')->get();
        return response()->json($venda);
    }

    public function store(Request $request)
    {
        $data = [
            'valor_venda'=>$request->valor_venda,
            'vendedor_id'=>$request->vendedor_id,
            'comissao'=> $request->valor_venda * 0.085
        ];
        $venda = Venda::create($data);
        return response()->json([
            'status' => 'success',
            'value'   => $venda
        ]);
    }

    public function show($id)
    {
        $venda = Venda::find($id);
        return response()->json($venda);
    }

    public function update(Request $request, $id)
    {
        $venda = Venda::find($id);
        $venda->update($venda->all());
        return response()->json([
            'status' => 'success',
            'value'   => $venda
        ]);
    }

    public function destroy($id)
    {
        $venda = Venda::find($id);
        $venda->delete();
        return response()->json('Venda Excluída');
    }

}
