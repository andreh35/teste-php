<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Sale;
use App\Models\User;
use Mail;

class RelatorioDeVendasDoDiaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relatorio:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia relatório de vendas ao fim do dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dados = Sale::where('created_at','like','%'.date('Y-m-d').'%')
            ->get();

        $valor = 0;
        if(count($dados)>0){
            foreach($dados as $soma){
                $valor = $valor + $soma->valor_venda;

            }
        }
        $html = 'Total de vendas do dia '.date('d/m/Y').' é R$ '.number_format($valor,2,",",".");
        Mail::raw($html, function ($mail) {
            $mail->from('remetente@email.com', 'Teste Dev')
            ->to('receptor@email.com')
            ->subject('Vendas do dia');
            //->setBody($html, 'text/html');
        });

        $this->info('Email com vendas do Dia enviado com sucesso.');
        \Log::info($html);

    }
}
