<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendedor_id',
        'valor_venda',
        'comissao',

    ];
    public function vendedor()
    {
        return $this->hasMany('App\Models\Vendedor');
    }

}
