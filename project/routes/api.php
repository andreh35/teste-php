<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\VendaController;
use App\Http\Controllers\Api\VendedorController;
use App\Http\Controllers\Api\VendedorvendaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {
    Route::resource('vendas', VendaController::class);
    Route::resource('vendedores',VendedorController::class);
    Route::resource('vendedorvenda',VendedorvendaController::class);
   });


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
