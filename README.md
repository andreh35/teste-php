## Teste para desenvolvedor PHP
Desafio de um projeto visando a candidatura a uma vaga de desenvolvedor PHP.

## O desafio
Desenvolver uma API para cadastro de vendas para vendedores e calcular a comissão dessas vendas (a comissão será de 8.5% sobre o valor da venda)
Ao final de cada dia deve ser enviado um email com um relatório com a soma de todas as vendas efetuadas no dia.
Criar uma aplicação que consuma essa api onde possamos utilizar todos os serviços dela.

* A API deverá ser escrita em PHP ​ (obrigatório)​ ;
* A aplicação pode ser escrita em Javascript ou PHP;
* O banco poderá ser modelado em MySQL/PostgreSQL;
* Poderá consultar a internet;
* Subir o projeto no GitHub e mandar o link.

## Estrutura do Projeto
Para desenvolvimento do projeto foi usado um contêiner docker com docker-compose que conteḿ as seguintes imagens:

* NGINX
* mysql:5.7.29
* PHP:8.0.5
* Phpmyadmin
* NPM com node:13.7
* composer
* Artisan
* MailHog

## Estrutura de pastas e arquivos
    .
    ├── ...
    ├── teste-dev-php              
    │   ├── mysql                  
    │   ├── nginx                  
    │   └── project               # pasta do projeto laravel             
    │   └── docker-compose.yml     
    │   └── docker-php-entrypoint  
    │   └── Dockerfile             
    │   └── README.md              
    └── ...

## A aplicação

* Aplicação foi desenvolvida utilizando VUE.js e Laravel 8 

## Getting Started

### Executando a aplicação

* É necessário Baixar o projeto para um diretorio local e execuar os seguintes comandos em um terminal:
```
docker-compose up -d --build
docker-compose artisan migrate
npm install vue-router vue-axios *opcional
npm install *opcional
```
* após executados os comandos acima, já é possível acessar o seu browser na porta 8081, para o projeto e 8082 para o  phpmyadmin;

 Ex:
 ```
 http://localhost:8081 [projeto]
 http://localhost/8082 [phpmyadmin]
 ```
obs: Pode ser necessário conceder permissão de escrita na pasta ./project

## Testando dos endpoints
É possível utilizar o aplicativo POSTMAN para testes das API's

## Executando o envio do relatório diário
Testar o envio de relatório:
 ```
docker-compose run artisan relatorio:cron
 ```
O Resultado poderá ser visto no seguinte arquivo:
 ```
 ./project/storage/logs/laravel.log
  ```

A Tarefa que executará o envio do relatório de vendas é a seguinte e pode ser executada também em um terminal:
 ```
docker-compose run artisan schedule:run
 ```
O Comando que deve ser adicionado na cron do seu sistema operaciona Linix para configurar o envio diário do relatório
 ```
crontab-e
 ```
 e adicionar a linha abaixo:
 ```
* * 18 * * docker-compose run artisan schedule:run >/dev/null 2>&1
 ```

